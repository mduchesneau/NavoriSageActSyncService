﻿namespace NavoriSageActSyncService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.actServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.actServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // actServiceProcessInstaller
            // 
            this.actServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.actServiceProcessInstaller.Password = null;
            this.actServiceProcessInstaller.Username = null;
            // 
            // actServiceInstaller
            // 
            this.actServiceInstaller.ServiceName = "NavoriActSyncService";
            this.actServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.actServiceProcessInstaller,
            this.actServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller actServiceProcessInstaller;
        private System.ServiceProcess.ServiceInstaller actServiceInstaller;
    }
}