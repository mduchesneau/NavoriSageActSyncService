﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NavoriSageActSyncService.MappingClasses
{
    public class FieldAssociation
    {
        // Attributes
        public String actFieldName { get; set; }
        public String extFieldName { get; set; }

        public ExtranetFieldMetaData extFieldMetaData { get; set; }
        

        /// <summary>
        /// Simple Constructor
        /// </summary>
        /// <param name="actField">Sage ACT Field Name</param>
        /// <param name="extField">Extranet Field Name</param>
        public FieldAssociation(String actField, String extField) {

            actFieldName = actField;
            extFieldName = extField;
        }

        /// <summary>
        /// Complete Constructor
        /// </summary>
        /// <param name="actField">Sage ACT Field Name</param>
        /// <param name="extField">Extranet Field Name</param>
        /// <param name="extMetaData">Extranet Field Metadata</param>
        public FieldAssociation(String _actField, String _extField, ExtranetFieldMetaData _extMetaData)
        {
            actFieldName = _actField;
            extFieldName = _extField;
            extFieldMetaData = _extMetaData;
        }



    }
}
