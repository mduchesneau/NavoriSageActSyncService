﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NavoriSageActSyncService.MappingClasses
{
    public class ExtranetFieldMetaData
    {

        // Attributes
        public String name { get; set; }
        public String display { get; set; }
        public String fieldType { get; set; }
        public Boolean isVisible { get; set; }
        public Boolean isReadOnly { get; set; }
        public int luid { get; set; }


        public ExtranetFieldMetaData(String _name, String _display, String _fieldType, Boolean _isVisible, Boolean _isReadOnly, int _luid)
        {
            name = _name;
            display = _display;
            fieldType = _fieldType;
            isVisible = _isVisible;
            isReadOnly = _isReadOnly;
            luid = _luid;
        }

    }
}
