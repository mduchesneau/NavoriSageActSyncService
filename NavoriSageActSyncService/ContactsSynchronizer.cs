﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Timers;
using System.IO;
using System.Threading;
using System.Configuration;
using Act.Framework;
using Act.Framework.Contacts;
using System.ComponentModel;
using System.Data.EntityClient;
using NavoriSageActSyncService.MappingClasses;
using System.Collections.Generic;
using System.Xml;
using System.Reflection;
using Act.Framework.Lookups;
using Act.Framework.Database;
using Act.Framework.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace NavoriSageActSyncService
{
    class ContactsSynchronizer
    {
        // Constants
        private double timerInterval = 36000.0;
        private bool logEnabled = true;
        private bool logLevelDebug = false;

        private static String FILE_LOG = @"NavoriSageActSync.log";
        private static String FILE_MAPPING = @"Importextranet.map";

        public static String CONFIG_REFRESH_INTERVAL = "refreshInterval";
        public static String CONFIG_FILE_FIELDMAP= "FieldMapFilePath";
        public static String CONFIG_SQL_SERVERNAME = "SqlServerName";
        public static String CONFIG_SQL_USERNAME = "SqlUserName";
        public static String CONFIG_SQL_PASSWORD = "SqlPassword";
        public static String CONFIG_SQL_DATABASE = "SqlDatabase";
        public static String CONFIG_ACT_SERVERNAME = "ActServerName";
        public static String CONFIG_ACT_USERNAME = "ActUserName";
        public static String CONFIG_ACT_PASSWORD = "ActPassword";
        public static String CONFIG_ACT_DATABASE = "ActDatabase";
        public static String CONFIG_LOG_ENABLED = "EnableLogs";
        public static String CONFIG_LOGLEVEL_DEBUG = "LogLevelDebug";

        public static String CONFIG_CSTRING_EXTRANET = "ExtranetNavoriEntities";


        public static String MAPPING_NODE_FIELDRECORDMAP = "FieldRecordMap";
        public static String MAPPING_NODE_RECORDMAP = "RecordMap";
        public static String MAPPING_NODE_FIELDMETADATA = "FieldMetaData";
        public static String MAPPING_NODE_FIELDMETADATA_NAME = "Name";
        public static String MAPPING_NODE_FIELDMETADATA_DISPLAY = "Display";
        public static String MAPPING_NODE_FIELDMETADATA_FIELDTYPE = "FieldType";
        public static String MAPPING_NODE_FIELDMETADATA_ISVISIBLE = "IsVisible";
        public static String MAPPING_NODE_FIELDMETADATA_ISREADONLY = "IsReadOnly";
        public static String MAPPING_NODE_FIELDMETADATA_LUID = "LUID";
        public static String MAPPING_NODE_ASSOCIATIONMAP = "AssociationMap";
        public static String MAPPING_NODE_ASSOCIATION = "Association";
        public static String MAPPING_NODE_ASSOCIATION_FIELD = "Field";
        public static String MAPPING_NODE_ASSOCIATION_FIELD_NAME = "Name";

        // Private elements
        System.Diagnostics.EventLog logMainEvents;

        private System.Timers.Timer timer;
        private Thread thread;

        private ActFramework framework;
        private ExtranetNavoriEntities extranetDb;

        List<FieldAssociation> assocs;

        // Constructor
        public ContactsSynchronizer(System.Diagnostics.EventLog log)
        {
            logMainEvents = log;
            //connectToActSDK();

            // Configure
            TimeSpan ts = new TimeSpan();
            bool res = TimeSpan.TryParse(ConfigurationManager.AppSettings[CONFIG_REFRESH_INTERVAL], out ts);
            if (res)
            {
                timerInterval = ts.TotalSeconds;
            }
            
            logEnabled = (ConfigurationManager.AppSettings[CONFIG_LOG_ENABLED].ToLower().CompareTo("true") == 0);
            logLevelDebug = (ConfigurationManager.AppSettings[CONFIG_LOGLEVEL_DEBUG].ToLower().CompareTo("true") == 0);

            //instantiate timer
            thread = new Thread(new ThreadStart(this.InitTimer));

            // DEBUGGING - FOR REFERENCE ONLY
            //serializeExampleData();
            //serializeExampleConfig();
        }

        // Start thread
        public void startPeriodicParsing()
        {

            thread.Start();

            synchronizeContacts();
        }

        // Stop thread
        public void stopPeriodicParsing()
        {
            thread.Abort();
            thread = new Thread(new ThreadStart(this.InitTimer));
        }

        // Execute once
        public void executeOnce()
        {
            synchronizeContacts();
        }


        // Publish to all logging sources
        private void publishLogEvent(String msg)
        {
            if (logEnabled)
            {
                // Write to console
                Console.WriteLine(msg);

                // Write to Event Viewer
                logMainEvents.WriteEntry(msg);

                // Publish to log file
                // SOURCE: http://stackoverflow.com/questions/3053123/how-can-i-write-line-by-line-in-txt-data
                using (StreamWriter sw = File.AppendText(FILE_LOG))
                {
                    sw.WriteLine(String.Format("[{0}] : {1}", DateTime.Now.ToString(), msg) );
                }
            }
        }


        // == PROCESSES == //

        // == Thread/Timer == //

        // Initialize the timer
        private void InitTimer()
        {
            timer = new System.Timers.Timer();
            //wire up the timer event 
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            //set timer interval   
            //var timeInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["TimerIntervalInSeconds"]); 
            timer.Interval = (timerInterval * 1000);
            // timer.Interval is in milliseconds, so times above by 1000 
            timer.Enabled = true;
        }

        // Reset the elapsed time
        protected void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                synchronizeContacts();
            }
            catch (Exception ex)
            {
                publishLogEvent("Exception on sync! \nError message: " + ex.Message);

            }
        }


        // ==== DATABASE CONNECTIONS ==== //

        /// <summary>
        /// Connect application to ACT Database & SDK functionality
        /// </summary>
        /// <returns>Connection result (True if successful)</returns>
        private bool connectToActSDK()
        {
            if (framework == null || !framework.IsLoggedOn)
            {
                framework = new ActFramework();
                String sServer = ConfigurationManager.AppSettings.Get(CONFIG_ACT_SERVERNAME);
                String sUser = ConfigurationManager.AppSettings.Get(CONFIG_ACT_USERNAME);
                String sPwd = ConfigurationManager.AppSettings.Get(CONFIG_ACT_PASSWORD);
                String sDb = ConfigurationManager.AppSettings.Get(CONFIG_ACT_DATABASE);

                framework.LogOn(sUser, sPwd, sServer, sDb);
            }

            return framework.IsLoggedOn;
        }


        /// <summary>
        /// Log off from the ACT framework
        /// </summary>
        private void disconnectFromActSDK()
        {
            if (framework == null || !framework.IsLoggedOn)
            {
                framework.LogOff();
            }
        }


        /// <summary>
        /// Connect to Extranet Database
        /// </summary>
        /// <returns>Connection result (True if successful)</returns>
        private bool connectToExtranetDB()
        {
            
            EntityConnectionStringBuilder sConn = new EntityConnectionStringBuilder(ConfigurationManager.ConnectionStrings[CONFIG_CSTRING_EXTRANET].ConnectionString);
            
            //sConn.Add("Data Source", ConfigurationManager.AppSettings.Get(CONFIG_SQL_SERVERNAME) );
            //sConn.Add("Provider", "System.Data.SqlClient");
            //sConn.Add("Initial Catalog", ConfigurationManager.AppSettings.Get(CONFIG_SQL_DATABASE));
            //sConn.Add("User id", ConfigurationManager.AppSettings.Get(CONFIG_SQL_USERNAME));
            //sConn.Add("Password", ConfigurationManager.AppSettings.Get(CONFIG_SQL_PASSWORD));
            EntityConnection eConn = new EntityConnection(sConn.ToString());
            
            extranetDb = new ExtranetNavoriEntities(eConn);

            return extranetDb != null;
        }


        // ==== ====


        // ==== Synchronization / Manipulation ==== //

        /// <summary>
        /// Drive the Data import and sync process
        /// </summary>
        private void synchronizeContacts()
        {
            bool result = false;
            
            publishLogEvent("Connecting to ACT Database...");
            result = connectToActSDK();
            //result = true;

            if (!result)
            {
                publishLogEvent("Could not connect to ACT Database!");
            }
            else
            {
                publishLogEvent("Connecting to Extranet Database...");
                result = connectToExtranetDB();

                if (!result)
                {
                    publishLogEvent("Could not connect to Extranet Database!");
                }
                else
                {

                    publishLogEvent("Fetching Contacts from Extranet Database...");
                    IQueryable<Valid52Users> contactList = fetchListFromExtranet();

                    publishLogEvent("Syncing Contacts to ACT Database...");
                    result = syncContactsToAct(contactList);

                    publishLogEvent("SYNC RESULT: " + (result?"SUCCESS":"FAILED") );
                }
            }

            // DEBUG
            //publishLogEvent("NavoriQLMF Feed Parser: Adding new TEST contact...");
            //addTestContactToAct();


            // Disconnect from ACT
            disconnectFromActSDK();
        }

        

        /// <summary>
        /// Query Extranet DB to get list of Contacts
        /// </summary>
        private IQueryable<Valid52Users> fetchListFromExtranet()
        {
            IQueryable<Valid52Users> contactList = null;

            // Query table
            var contactSelect = from oEntity in extranetDb.Valid52Users
                           select oEntity;
            int rowCount = contactSelect.Count();

            //DEBUG
            if (logLevelDebug)
            {
                publishLogEvent("Rows fetched from Extranet DB: " + rowCount);
            }

            // Store list in enumerable object
            if (rowCount != 0)
            {
                if (contactSelect is IQueryable<Valid52Users>)
                {
                    contactList = (IQueryable<Valid52Users>)contactSelect;
                }
            }

            return contactList;
        }


        /// <summary>
        /// Sync fetched Extranet Contacts to ACT database
        /// </summary>
        /// <param name="exContactList"></param>
        /// <returns></returns>
        private bool syncContactsToAct(IQueryable<Valid52Users> exContactList)
        {
            bool result = true;
            if (exContactList != null && framework != null && framework.IsLoggedOn)
            {
                // Load mapping file
                assocs = loadMappingData();

                // Go through each Extranet contact entry
                foreach (Valid52Users exContact in exContactList) 
                {
                    // Log current contact
                    publishLogEvent(String.Format("Extranet Contact: {0}, {1} {2};", exContact.Company_Name, exContact.First_Name, exContact.Last_Name));
                    
                    // Do not Sync Navori contacts
                    if (exContact.Email.Contains("@navori.com"))
                    {
                        publishLogEvent(String.Format("SYNC SKIPPED -- Contact is from Navori"));
                    }
                    else
                    {

                        //PropertyDescriptor pdCompany = framework.Contacts.GetContactFieldDescriptor("TBL_CONTACT.COMPANYNAME");
                        //PropertyDescriptor pdEmail = framework.Contacts.GetContactFieldDescriptor("TBL_CONTACT.BUSINESS_EMAIL");
                        PropertyDescriptor pdExtId = framework.Contacts.GetContactFieldDescriptor("TBL_CONTACT.CUST_ExtranetID_070645118");
                        //PropertyDescriptor pdFirstName = framework.Contacts.GetContactFieldDescriptor("TBL_CONTACT.FIRSTNAME");
                        //PropertyDescriptor pdLastName = framework.Contacts.GetContactFieldDescriptor("TBL_CONTACT.LASTNAME");

                        // Make sure PropertyDescriptors are successfully found
                        if (pdExtId == null)
                        {
                            publishLogEvent("Contacts Sync: Could not retrieve ACT Property Descriptors from CONTACT class!");
                            result = false;
                        }
                        else
                        {
                            // Query ACT DB to see if contact already exists
                            ContactFieldDescriptor fdExtId = framework.Contacts.GetContactFieldDescriptor("TBL_CONTACT.CUST_ExtranetID_070645118");
                            ContactLookup lookup = framework.Lookups.LookupContactsReplace(exContact.UserId.ToString(), OperatorEnum.EqualTo, fdExtId, true, true);
                            ContactList actContactList = lookup.GetContacts(null);

                            // Select Contact found or create a new one
                            Contact syncContact = null;
                            if (actContactList.Count > 0)
                            {
                                //DEBUG
                                if (logLevelDebug)
                                {
                                    publishLogEvent("   ACT Contact FOUND");
                                }

                                syncContact = (Contact)actContactList.First();

                                //framework.Contacts.
                            }
                            else
                            {
                                //DEBUG
                                if (logLevelDebug)
                                {
                                    publishLogEvent("   ACT Contact NOT FOUND");
                                }

                                syncContact = addContactToAct();
                            }

                            // Perform Data Sync
                            syncContact = updateContactFromExtranetData(exContact, syncContact);

                            if (syncContact == null)
                            {
                                publishLogEvent("Contacts Sync: ERROR during sync; ACT Contact object is NULL");
                                result = false;
                            }

                        }
                    }
                }
            }

            return result;
        }


        /// <summary>
        /// Update ACT contact from the source extranet contact data
        /// </summary>
        /// <param name="exContact">Source data from Extranet DB</param>
        /// <param name="actContact">ACT Contact to update</param>
        /// <returns>The updated Contact object</returns>
        private Contact updateContactFromExtranetData(Valid52Users exContact, Contact actContact)
        {
            if (actContact != null && exContact != null)
            {
                // Go through each field and assign data
                foreach (FieldAssociation assoc in assocs)
                {
                    // Convert field name to Entity-converted name
                    String propName = assoc.extFieldName;
                    if (propName.Equals("User ID") || propName.Equals("Address 1") || propName.Equals("Address 2"))
                    {
                        propName = propName.Replace(" ","").Replace('D','d');
                    }
                    else
                    {
                        propName = propName.Replace('?', '_').Replace('(', '_').Replace(')', '_').Replace(' ', '_').Replace('=', '_')
                                                            .Replace('/', '_').Replace('-', '_').Replace('#', '_').Replace('.', '_').Replace(':', '_');
                    }

                    // Get list of attributes (Properties)
                    Type entityType = exContact.GetType();
                    PropertyInfo[] entityPropList = entityType.GetProperties();


                    // Find corresponding Entity Property Name
                    foreach (PropertyInfo pi in entityPropList)
                    {
                        if (pi.Name == propName)
                        {
                            // Prepare ACT field
                            String sActFieldName = "TBL_CONTACT." + assoc.actFieldName;
                            ContactFieldDescriptor cfd = framework.Contacts.GetContactFieldDescriptor(sActFieldName);
                            FieldDescriptorCollection cField = framework.Fields.GetFields(RecordType.Contact);
                            FieldDescriptor fd = cField.FindByColumnName(assoc.actFieldName);

                            if (cfd != null && !cfd.IsReadOnly && fd != null)
                            {
                                // Prepare value
                                String value = ((pi.GetValue(exContact, null) == null) ? "" : pi.GetValue(exContact, null).ToString());
                                int fieldSize = (fd.GetLength() <= 0 ? 50 : fd.GetLength());
                                if (value.Length >= fieldSize)
                                {
                                    value = value.Remove(fieldSize - 1);
                                }
                                
                                // Convert data type if necessary
                                if (fd != null && fd.FieldDataType == FieldDataType.Decimal)
                                {
                                    Decimal dVal = 0;
                                    bool res = Decimal.TryParse(value, out dVal);
                                    cfd.SetValue(actContact, dVal);
                                }
                                //else if (fd.FieldDataType == FieldDataType.)
                                else
                                {
                                    // Assign field value to ACT Contact
                                    cfd.SetValue(actContact, value);
                                }
                            }
                            break;
                        }
                    }
                }

                // Apply Changes
                actContact.Update();
            }

            return actContact;
        }


        /// <summary>
        /// Add and return a New Contact to DB
        /// </summary>
        private Contact addContactToAct()
        {
            ContactList priContactsList = framework.Contacts.GetContacts(null);
            
            return (Contact)((IBindingList)priContactsList).AddNew();
        }


        /// <summary>
        /// Create a test contact and adds it to DB
        /// </summary>
        private void addTestContactToAct()
        {
            string firstName = "TestJohn";
            string lastName = "TestSmith";
            string title = "TestAccountant";
            string company = "ABC123 inc";

            Contact actContact = addContactToAct();
            actContact.FullName = firstName + " " + lastName;
            actContact.Fields["Contact.Title", false] = title;
            actContact.Company = company;

            actContact.Update();

        }

        /// <summary>
        /// Read through mapping file and fetch association data
        /// </summary>
        /// <returns>List of associations</returns>
        private List<FieldAssociation> loadMappingData()
        {
            List<FieldAssociation> assocs = new List<FieldAssociation>();
            List<ExtranetFieldMetaData> fieldMeta = new List<ExtranetFieldMetaData>();

            Boolean isInFieldRecordMap = false;
            Boolean isInRecordMap = false;
            Boolean isInAssociationMap = false;
            Boolean isInAssociation = false;

            using (XmlReader myReader = XmlReader.Create(FILE_MAPPING))
            {
                while (myReader.Read())
                {
                    switch (myReader.NodeType)
                    {
                        case XmlNodeType.Element:
                            // FieldRecordMap elements
                            /*if (myReader.Name.Equals(MAPPING_NODE_FIELDRECORDMAP))
                            {
                                isInFieldRecordMap = true;
                            }
                            else if (myReader.Name.Equals(MAPPING_NODE_RECORDMAP))
                            {
                                isInRecordMap = true;
                            }
                            else if (isInRecordMap)
                            {*/
                                if (myReader.Name.Equals(MAPPING_NODE_FIELDMETADATA))
                                {
                                    try
                                    {
                                        String name = myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_NAME);
                                        String display = myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_DISPLAY);
                                        String fieldType = myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_FIELDTYPE);
                                        bool isVisible = false;
                                        bool res = Boolean.TryParse(myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_ISVISIBLE), out isVisible);
                                        bool isReadOnly = false;
                                        Boolean.TryParse(myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_ISREADONLY), out isReadOnly);
                                        int luid = Int32.Parse(myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_LUID));


                                        ExtranetFieldMetaData fmd = new ExtranetFieldMetaData(name, display, fieldType, isVisible, isReadOnly, luid);

                                        fieldMeta.Add(fmd);
                                   }
                                    catch (FormatException e)
                                    {
                                        if (logLevelDebug) {
                                            publishLogEvent(String.Format("Parse Error : {0} : {1} : {2} ", MAPPING_NODE_FIELDMETADATA, myReader.GetAttribute(MAPPING_NODE_FIELDMETADATA_NAME), e.Message));
                                        }
                                    }
                                }
                            /*}
                            // AssociationMap elements
                            else if (myReader.Name.Equals(MAPPING_NODE_ASSOCIATIONMAP))
                            {
                                isInAssociationMap = true;
                            }
                            else if (myReader.Name.Equals(MAPPING_NODE_ASSOCIATION))
                            {
                                isInAssociation = true;
                            }
                            else if (isInAssociation)
                            {*/
                                else if (myReader.Name.Equals(MAPPING_NODE_ASSOCIATION_FIELD))
                                {
                                    String extName = myReader.GetAttribute(MAPPING_NODE_ASSOCIATION_FIELD_NAME);
                                    String actName = "";

                                    // Fetch second field
                                    do
                                    {
                                        myReader.Read();
                                    } while (myReader.NodeType != XmlNodeType.Element);
                                    if (myReader.Name.Equals(MAPPING_NODE_ASSOCIATION_FIELD))
                                    {
                                        actName = myReader.GetAttribute(MAPPING_NODE_ASSOCIATION_FIELD_NAME);
                                    }

                                    // New association
                                    ExtranetFieldMetaData meta = fieldMeta.Find(o => o.name == extName);
                                    FieldAssociation assoc = new FieldAssociation(actName, extName, meta);

                                    // Add to list
                                    assocs.Add(assoc);
                                }
                            /*}
                            else if (isInFieldRecordMap)
                            {

                            }*/
                            break;
                        case XmlNodeType.EndElement:
                            // FieldRecordMap elements
                            if (myReader.Name.Equals(MAPPING_NODE_FIELDRECORDMAP))
                            {
                                isInFieldRecordMap = false;
                            }
                            else if (myReader.Name.Equals(MAPPING_NODE_RECORDMAP))
                            {
                                isInRecordMap = false;
                            }
                            // AssociationMap elements
                            else if (myReader.Name.Equals(MAPPING_NODE_ASSOCIATIONMAP))
                            {
                                isInAssociationMap = false;
                            }
                            else if (myReader.Name.Equals(MAPPING_NODE_ASSOCIATION))
                            {
                                isInAssociation = false;
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            return assocs;
        }


        // ==== TOOLS ====

        /// <summary>
        /// Reverse characters in a string
        /// Source: http://stackoverflow.com/questions/228038/best-way-to-reverse-a-string
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
