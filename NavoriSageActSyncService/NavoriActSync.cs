﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Reflection;
using System.IO;

namespace NavoriSageActSyncService
{
    public partial class NavoriActSync : ServiceBase
    {
        // Constants
        public static String LOG_MAIN_EVENTS_SOURCE = "NavoriSageACTSync_LogSource";
        public static String LOG_MAIN_EVENTS_LOG = "NavoriSageACTSync_Log";

        public static String CONFIG_REFRESH_INTERVAL = "RefreshInterval";
        


        ContactsSynchronizer contactsSynchronizer;


        // Main method
        // Console Strategy SOURCE: http://einaregilsson.com/run-windows-service-as-a-console-program/
        static void Main(string[] args)
        {
            NavoriActSync service = new NavoriActSync();

            if (Environment.UserInteractive)
            {
                service.OnStart(args);
                Console.WriteLine("Press any key to stop program");
                Console.Read();
                service.OnStop();
            }
            else
            {
                ServiceBase.Run(service);
            }
        }

        // Constructor
        public NavoriActSync()
        {
            InitializeComponent();

            // Initialize log
            if (!System.Diagnostics.EventLog.SourceExists("NavoriSageACTSyncService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "NavoriSageACTSyncService", "NavoriSageACTSync");
            }
            logMainEvents.Source = "NavoriSageACTSyncService";
            logMainEvents.Log = "NavoriSageACTSync";
        }


        // == LIFECYCLE == //


        protected override void OnStart(string[] args)
        {
            try
            {
                logMainEvents.WriteEntry("Current Directory: " + Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));
                System.IO.Directory.SetCurrentDirectory(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location));

                // Create parser instance
                contactsSynchronizer = new ContactsSynchronizer(logMainEvents);

                // Execute
                //contactsSynchronizer.executeOnce();
                contactsSynchronizer.startPeriodicParsing();

                logMainEvents.WriteEntry("Navori-SageACT Contacts Synchronizer: Service Started");
            }
            catch (Exception e)
            {
                logMainEvents.WriteEntry("NavoriQLMF Feed Parser: Exception on start!");
                logMainEvents.WriteEntry(e.Message);

            }
        }

        protected override void OnStop()
        {


            logMainEvents.WriteEntry("NavoriQLMF Feed Parser: Service Stopped");
        }

        protected override void OnContinue()
        {


        }




    }
}
